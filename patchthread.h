#pragma once

#include <QThread>
#include <QFileInfo>
#include <QFile>

#define PE_POINTER_OFFSET 0x3C
#define LARGE_ADDRESS_AWARE 0x20

class patchThread : public QThread {
    Q_OBJECT

private:
    QString path = "";
    QStringList filter;

    bool isValidExe(QFile &f, quint32 &flagsOff);
    void applyPatch(const QString &path);
    void makeBackup(const QFileInfo &finfo);

public:
     patchThread();

    void run() override;
    void setPath(const QString &path);

signals:
    void progress(const QString &file, int result);
};
