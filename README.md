# Large Address Aware Enabler (LAAE) #

Cross-platform application to enable LAA bit

# Usage
* Select _Executable_ if you want to patch a single executable or _Folder_ to select the application folder
* Click _Apply_ to patch your application

# Notes
LAAE can patch 32bit .exe and .dll if marked as executables

If _Folder_ mode is selected, it will try to patch every .exe and .dll in the specified folder and its subfolders

The _Folder_ mode does not support symlinks