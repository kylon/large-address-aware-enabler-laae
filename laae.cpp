#include "laae.h"
#include "./ui_laae.h"

#include <QFileDialog>

LAAE::LAAE(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::LAAE)
{
    ui->setupUi(this);
    ui->exeMode->setChecked(true);

    progBox = new QMessageBox(this);
    applyThd = new patchThread();
    cleanThd = new cleanupThread();

    progBox->setWindowTitle("LAA patch");
    progBox->setIcon(QMessageBox::Information);

    connect(progBox, SIGNAL(buttonClicked(QAbstractButton *)), this, SLOT(onProgBoxBtnClick(QAbstractButton *)));
    connect(applyThd, SIGNAL(started()), this, SLOT(onApplyThdStart()));
    connect(applyThd, SIGNAL(finished()), this, SLOT(onApplyThdEnd()));
    connect(applyThd, SIGNAL(progress(const QString &, int)), this, SLOT(onApplyThdProgressUpd(const QString &, int)));
    connect(cleanThd, SIGNAL(restoreComplete()), this, SLOT(onRestoreComplete()));
}

LAAE::~LAAE() {
    quitApplyThd();
    quitCleanThd();
    applyThd->deleteLater();
    cleanThd->deleteLater();

    delete progBox;
    delete ui;
}

void LAAE::on_browseBtn_clicked() {
    bool isFileMode = ui->exeMode->isChecked();
    QString caption = tr("Browse...");
    QString path;

    if (isFileMode)
        path = QFileDialog::getOpenFileName(this, caption, tr(""), tr("Exe (*.exe);;DLL (*.dll);;All files (*)"));
    else
        path = QFileDialog::getExistingDirectory(this, caption, "");

    ui->fpath->setText(path);
}

void LAAE::on_applyBtn_clicked() {
    QMessageBox::StandardButton patchAlert;

    patchAlert = QMessageBox::question(this, tr("Apply patch"), tr("Do you want to apply LAA patch?"));

    if (patchAlert == QMessageBox::No)
        goto out;

    this->errors = 0;
    this->patchedC = 0;
    this->processedC = 0;
    this->userPath = ui->fpath->text();
    this->cancel = false;

    ui->fullLogBox->clear();
    applyThd->setPath(userPath);
    applyThd->start();
    ui->applyBtn->setEnabled(false);

out:
    ui->fullLogBox->clear();
    ui->fpath->setText("");
    return;
}

void LAAE::onApplyThdStart() {
    progBox->setText(tr("Patching.."));
    progBox->setStandardButtons(QMessageBox::Cancel);
    progBox->exec();
}

void LAAE::onApplyThdEnd() {
    QString err = QString("Patched! (%1 errors)").arg(this->errors);
    QString patchedMsg = QString("Patched: %1/%2 files.").arg(this->patchedC).arg(this->processedC);

    this->quitApplyThd();
    ui->fullLogBox->appendPlainText(patchedMsg);
    progBox->setText(tr(err.toStdString().c_str()));
    progBox->setStandardButtons(QMessageBox::Ok);
}

void LAAE::onApplyThdProgressUpd(const QString &file, int result) {
    bool success = result == 0;
    bool error = result == 1;
    bool invalidExe = result == 2;
    QString statMsg( error ? "[I/O Error]" : (invalidExe ? "[Skip]":"[Ok]") );

    ui->fullLogBox->appendPlainText("Patching " + file + "...\n" + statMsg+ "\n");
    ++this->processedC;

    if (error)
        ++this->errors;

    if (success)
        ++this->patchedC;
}

void LAAE::onRestoreComplete() {
    ui->fullLogBox->clear();

    connect(applyThd, SIGNAL(finished()), this, SLOT(onApplyThdEnd()));
}

void LAAE::onProgBoxBtnClick(QAbstractButton *button) {
    if (button == nullptr || this->cancel || progBox->standardButton(button) == QMessageBox::Ok)
        return;

    this->cancel = true;

    disconnect(applyThd, SIGNAL(finished()), this, SLOT(onApplyThdEnd()));

    applyThd->terminate();
    this->quitApplyThd();

    cleanThd->setPath(userPath);
    cleanThd->run();
}

void LAAE::quitApplyThd() {
    applyThd->quit();
    applyThd->wait();
}

void LAAE::quitCleanThd() {
    cleanThd->quit();
    cleanThd->wait();
}

void LAAE::on_fpath_textChanged(const QString &arg1) {
    if (arg1 == "")
        return;

    ui->applyBtn->setEnabled(true);
}
