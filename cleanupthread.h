#pragma once

#include <QThread>
#include <QFile>

class cleanupThread: public QThread {
    Q_OBJECT

private:
    QString path = "";
    QStringList filter;

public:
    cleanupThread();

    void run() override;
    void setPath(const QString &path);
    void restore(const QString &path);

signals:
    void restoreComplete();
};
