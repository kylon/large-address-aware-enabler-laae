#include "patchthread.h"

#include <QDirIterator>
#include <QDataStream>

patchThread::patchThread() {
    filter << "*.exe" << "*.dll";
}

void patchThread::run() {
    if (this->path == "")
        return;

    applyPatch(this->path);
}

void patchThread::setPath(const QString &path) {
    this->path = path;
}

bool patchThread::isValidExe(QFile &file, quint32 &flagsOff) {
    QByteArray sign, peoff;
    quint32 pe_off = 0U;

    file.seek(0);
    sign = file.read(sizeof(quint16));

    file.seek(PE_POINTER_OFFSET);
    peoff = file.read(sizeof(quint32));

    for (int i=peoff.length()-1; i>=0; --i)
        pe_off += static_cast<quint32>(static_cast<quint8>(peoff[i]) << (8*i));

    file.seek(pe_off);
    sign.append(file.read(sizeof(quint16)));

    flagsOff = file.pos();

    return sign.toStdString() == "MZPE";
}

void patchThread::applyPatch(const QString &path) {
    QFileInfo finfo(path);

    if (finfo.isFile()) {
        quint32 flagsOff = 0U;
        quint8 flags = 0U;
        QFile f(path);

        if (not f.open(QIODevice::ReadWrite)) {
            emit progress(path, 1);
            return;
        }

        if (!isValidExe(f, flagsOff)) {
            emit progress(path, 2);
            return;
        }

        QDataStream in(&f);

        this->makeBackup(finfo);
        f.seek(flagsOff + 20);

        in >> flags;

        flags |= LARGE_ADDRESS_AWARE;

        f.seek(flagsOff + 20);
        in << flags;
        f.close();

        emit progress(path, 0);

    } else if (finfo.isDir()) {
        QDirIterator dit(path, filter, QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);

        while (dit.hasNext())
            applyPatch(dit.next());
    }

    return;
}

void patchThread::makeBackup(const QFileInfo &finfo) {
    QString file(finfo.absoluteFilePath());
    QString fcp(file + ".orig");

    if (QFile::exists(fcp))
        return;

    QFile::copy(file, fcp);
}
