#include "cleanupthread.h"

#include <QFileInfo>
#include <QDirIterator>

cleanupThread::cleanupThread() {
    filter << "*.orig";
}

void cleanupThread::setPath(const QString &path) {
    this->path = path;
}

void cleanupThread::run() {
    if (path == "") {
        emit restoreComplete();
        return;
    }

    this->restore(this->path);
    emit restoreComplete();
}

void cleanupThread::restore(const QString &path) {
    QFileInfo finfo(path);

    if (finfo.isFile()) {
        QString bak(path);

        if (finfo.size() == 0)
            return;

        QFile::remove(path);
        QFile::rename(bak, path);

    } else if (finfo.isDir()) {
        QDirIterator dit(path, filter, QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);

        while (dit.hasNext())
            this->restore(dit.next());
    }

    return;
}
