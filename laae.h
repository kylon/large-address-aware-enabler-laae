#pragma once

#include "patchthread.h"
#include "cleanupthread.h"

#include <QMainWindow>
#include <QFile>
#include <QThread>
#include <QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui { class LAAE; }
QT_END_NAMESPACE

class LAAE : public QMainWindow {
    Q_OBJECT

public:
    LAAE(QWidget *parent = nullptr);
    ~LAAE();

private slots:
    void on_browseBtn_clicked();
    void on_applyBtn_clicked();
    void onApplyThdStart();
    void onApplyThdEnd();
    void onApplyThdProgressUpd(const QString &file, int result);
    void onRestoreComplete();
    void onProgBoxBtnClick(QAbstractButton *button);
    void on_fpath_textChanged(const QString &arg1);

private:
    Ui::LAAE *ui;
    patchThread *applyThd = nullptr;
    cleanupThread *cleanThd = nullptr;
    QMessageBox *progBox = nullptr;
    QString userPath;
    int errors;
    int patchedC;
    int processedC;
    bool cancel;

    void quitApplyThd();
    void quitCleanThd();
};
