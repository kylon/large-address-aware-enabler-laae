#include "laae.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LAAE w;
    w.setFixedSize(QSize(w.width(), w.height()));
    w.show();
    return a.exec();
}
